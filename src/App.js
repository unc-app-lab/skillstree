import './App.css';
import React, { useState, useEffect, Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Tabletop from "tabletop";
import Table from 'react-bootstrap/Table';
import Search from './Search';
import { BrowserRouter as Router } from "react-router-dom";
import Announcer from './announcer';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import parse from "html-react-parser";
import { stripHtml } from "string-strip-html";
import Papa from "papaparse";

function App() {

  const [data, setData] = useState([]);

  // Using papaparse to pull data from sheets in the form of a csv (more sustainable)
  useEffect(() => {
    Papa.parse('https://docs.google.com/spreadsheets/d/e/2PACX-1vTttdmJhe9Cg6QIIgK9YMcwZyHvyP4_F4xn8tYSL6ZNmcYwcUaFfUOD-jbbrklb4At7Ej8wG5y1OwUR/pub?gid=498153196&single=true&output=csv', {
        download: true,
        header: true,
        complete: function(results) {
          var data = results.data
          setData(data);
        }
    })
  }, []);

  //stores the url of the current website
  const { search } = window.location;

  //stores the search query
  const query = new URLSearchParams(search).get('s');

  const [searchQuery, setSearchQuery] = useState(query || '');
  console.log(searchQuery);
  //creates an array of only relevant rows
  let copy = [...data];
  let filteredPosts = filterPosts(copy, searchQuery);

  // Graduation Date filtering
  const today = new Date(); // current date
  filteredPosts = filteredPosts.filter(isNotGraduated); // filters post based on graduation date mentioned
  
  function isNotGraduated(fp){  // function compares current date with graduation date
    let mdy = fp.GradDate.toString().split('/');
    if (mdy[2] < today.getFullYear()){
      return false;
    }
    if (mdy[2] == today.getFullYear() && mdy[0] < today.getMonth()){
      return false;
    }
    else {
      return true;
    }
  }

  // if (filteredPosts[0]) {
  //   console.log(filteredPosts[0].Website);
  // }
  // after getting data this displays each item 

  return (
    <>
      <Router>
        <div>
          {/* Allows screenreaders to know that the table has changed */}
          <Announcer message={`Table has ${filteredPosts.length} rows`} />

          <div class="row">
            <div class="column left">
              <h1>UNC App Lab Skills Tree</h1>
              <Search
                searchQuery={searchQuery}
                setSearchQuery={setSearchQuery}
              />
            </div>
            <div class="column right">
              <p>The Skills Tree is a resource to contact peers regarding questions or interests around a specific technology or area. If you would like to be included in this list and are open to being contacted about the skills you list, please fill out this form <a href="https://forms.gle/Tujwhgu6GEsY52zC6">here</a>.</p>
              <p>If you want to learn more about the App Lab and the services we provide, join our slack at <a href="https://unc-app-lab.slack.com/join/signup#/">this link</a> and check out our website <a href="https://applab.unc.edu/">here</a>.</p>
            </div>
          </div>

          <Table bordered striped responsive>
            <thead>
              <th> Name </th>
              <th> Email </th>
              <th> OS </th>
              <th> Coding Language </th>
              <th> IDEs </th>
              <th> Frameworks </th>
              <th> Application </th>
              <th> Additional Info </th>
            </thead>
            <tbody>
              {filteredPosts.map((item, i) => {
                return (
                  <Fragment key={i}>
                    <tr>
                      <td>{item.Name}</td>
                      <td>{item.Email}</td>
                      {/* Parse function is necessary for the html span tag to show up*/}
                      <td>{parse(item.Concat_Operating_System)}</td>
                      <td>{parse(item.Concat_Coding_Language)}</td>
                      <td>{parse(item.Concat_IDE)}</td>
                      <td>{parse(item.Concat_Framework)}</td>
                      <td>{parse(item.Concat_Application)}</td>
                      <td>{parse(splitHrefTag(item.Website))}</td>
                    </tr>
                  </Fragment>
                )
              })}
            </tbody>
          </Table>

        </div>
      </Router>
    </>
  );

}

// Removes previous href tags on links to clean it up
const removeHrefTag = (url) => {

  let closingTag = "</a>";

  if (url.includes("<a href='")) {
    let fullOpeningIndex = url.indexOf(">") + 1;
    return url.slice(fullOpeningIndex, url.length - closingTag.length);
  }

  return url;
}

// Adds href tag to each individual link
const splitHrefTag = (urls) => {
  let hrefArray = urls.split(",");
  let newString = "";

  for (let i = 0; i < hrefArray.length; i++) {

    hrefArray[i] = hrefArray[i].trim();

    //removes any previous href tags to keep things clean
    hrefArray[i] = removeHrefTag(hrefArray[i]).trim();

    hrefArray[i] = '<a href="' + hrefArray[i] + '"> ' + hrefArray[i] + " </a>";

    if (i == 0) {
      newString = hrefArray[i];
    } else {
      newString = newString + ", " + hrefArray[i];
    }

  }
  return newString
}


// Function made to remove all the span tags added to search items before filtered
// Has to be done before filtering

const removeSpanTag = (posts) => {
  let stringOpTag = "<span class='highlighted'>";
  let stringClsTag = "</span>";
  for (let i = 0; i < posts.length; i++) {
    for (let key in posts[i]) {
      if (posts[i][key].includes(stringOpTag)) {
        posts[i][key] = posts[i][key].slice(stringOpTag.length, posts[i][key].length - stringClsTag.length);
      }
    }
  }

  return posts;
}

const filterPosts = (posts, query) => {
  posts = removeSpanTag(posts);

  if (!query) {
    return posts
  };

  return posts.filter((post) => {

    // seperate array of terms is created to restrict searches in those columns

    const searchQuery = query.toLowerCase().trim();
    let concatArray = new Array();


    const codingLanguage = post.Concat_Coding_Language.toLowerCase();
    const application = post.Concat_Application.toLowerCase();
    const framework = post.Concat_Framework.toLowerCase();
    const ide = post.Concat_IDE.toLowerCase();
    const operatingsystem = post.Concat_Operating_System.toLowerCase();

    concatArray.push(stripHtml(codingLanguage).result);
    concatArray.push(stripHtml(application).result);
    concatArray.push(stripHtml(framework).result);
    concatArray.push(stripHtml(ide).result);
    concatArray.push(stripHtml(operatingsystem).result);

    //check if search term is present
    for (let i = 0; i < concatArray.length; i++) {
      if (concatArray[i].includes(searchQuery)) {
        return true;
      }
    }

    return false;
  }).map((item) => {

    //This is the section of code that allows for highlighting
    //React doesn't normally allow for scripts to be read through strings
    //Might be necessary for additional sanitazation. 
    let stringOpTag = "<span class='highlighted'>";
    let stringClsTag = "</span>";

    const searchQuery = query.toLowerCase().trim();
    let concatArray = new Array();

    const codingLanguage = item.Concat_Coding_Language.toLowerCase();
    const application = item.Concat_Application.toLowerCase();
    const framework = item.Concat_Framework.toLowerCase();
    const ide = item.Concat_IDE.toLowerCase();
    const operatingsystem = item.Concat_Operating_System.toLowerCase();

    concatArray.push(codingLanguage);
    concatArray.push(application);
    concatArray.push(framework);
    concatArray.push(ide);
    concatArray.push(operatingsystem);

    //Checks to see if the term is present in the row usin concat array
    //Adds highlighting to entire cell of original content to preserve casing
    for (let i = 0; i < concatArray.length; i++) {


      if (concatArray[i].includes(searchQuery)) {
        switch (i) {
          case 0:
            item.Concat_Coding_Language = stringOpTag + item.Concat_Coding_Language + stringClsTag;
            break;
          case 1:
            item.Concat_Application = stringOpTag + item.Concat_Application + stringClsTag;
            break;
          case 2:
            item.Concat_Framework = stringOpTag + item.Concat_Framework + stringClsTag;
            break;
          case 3:
            item.Concat_IDE = stringOpTag + item.Concat_IDE + stringClsTag;
            break;
          case 4:
            item.Concat_Operating_System = stringOpTag + item.Concat_Operating_System + stringClsTag;
            break;
          default:
            break;
        }
      }
    }

    return item;
  }

  );

};

export default App;
